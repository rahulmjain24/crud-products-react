import React from 'react'
import axios from 'axios'
import {
  Route,
  BrowserRouter as Router, Switch
} from "react-router-dom"
import './App.css'

import FlexContainer from './components/FlexContainer/FlexContainer'
import Loader from './components/Loader/Loader'
import Error from './components/Error/Error'
import ProductItem from './components/ProductItem/ProductItem'
import NavBar from './components/NavBar/NavBar'
import Product from './components/Product/Product'
import CreateProduct from './components/CreateProduct/CreateProduct'

class App extends React.Component {
  constructor(props) {
    super(props)

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    }

    this.state = {
      products: [],
      status: this.API_STATES.LOADING,
      error: ''
    }

    this.PRODUCTS = 'https://fakestoreapi.com/products/'
  }

  fetchUsingUrl = (url) => {
    this.setState({
      status: this.API_STATES.LOADING
    }, () => {
      axios.get(url)
        .then((response) => {
          console.log(response.data)
          this.setState({
            status: this.API_STATES.LOADED,
            products: response.data
          })
        })
        .catch((err) => {
          console.error(err)
          this.setState({
            status: this.API_STATES.ERROR,
            error: "An API error has occured!!!"
          })
        })
    })
  }

  edit = (id,title='',description='',category='',price='') => {
    const productsCopy = this.state.products
    const productToEdit = productsCopy.find((product) => {
      return product.id === id
    })
    if(title !== '') {
      productToEdit.title = title
    }
    if(description !== '') {
      productToEdit.description = description
    }
    if(category !== '') {
      productToEdit.category = category
    }
    if(price !== '') {
      productToEdit.price = price
    }
    this.setState({ products: productsCopy })
  }

  delete = (id) => {
    const productsCopy = this.state.products
    let productToDelete = productsCopy.find((product) => {
      return product.id === id
    })
    productsCopy.splice(productsCopy.indexOf(productToDelete), 1)
    this.setState({ products: productsCopy })
  }

  create = (data) => {
    const productsCopy = this.state.products
    const productToMake = { ...data }
    productToMake['id'] = productsCopy.length + 1000 + 1
    productToMake['rating'] = {}
    productToMake['rating']['count'] = 0
    productToMake['rating']['rate'] = 0
    productToMake['image'] = `https://contents.mediadecathlon.com/p1484240/ab565f3675dbdd7e3c486175e2c16583/p1484240.jpg`
    productsCopy.push(productToMake)
    console.log(productToMake)
    this.setState({ products: productsCopy })
  }

  componentDidMount() {
    this.fetchUsingUrl(this.PRODUCTS)
  }

  Products = () => {
    return this.state.products.map((product) => {
      return (
        <ProductItem
          key={product.id}
          {...product}
          edit={this.edit}
          delete={this.delete}
        />
      )
    })
  }

  render() {
    return (
      <div className="App">
        <Router>
          <NavBar />
          <FlexContainer className="column">
            {this.state.status === this.API_STATES.LOADING && <Loader />}
            {this.state.status === this.API_STATES.ERROR && <Error>{this.state.error}</Error>}
            {this.state.status === this.API_STATES.LOADED && this.state.products.length === 0 &&
              <Switch>
                <Route exact path="/create" render={() => {
                  return <CreateProduct create={this.create} />
                }} />
                <Route path="/" render={() => {
                  return <Error>No products available</Error>
                }} />
              </Switch>
            }
            {this.state.status === this.API_STATES.LOADED && this.state.products.length > 0 &&
            <Switch>
                <Route exact path="/" component={this.Products} />
                <Route exact path="/create" render={() => {
                  return <CreateProduct create={this.create} />
                }} />
                <Route exact path="/:id" render={(props) => {
                  const id = props.match.params.id
                  const productToPass = this.state.products.find((product) => {
                    return product.id.toString() === id
                  })
                  return <Product {...props} {...productToPass} edit={this.edit} delete={this.delete} />
                }} />
              </Switch>
            }
          </FlexContainer>
        </Router>
      </div>
    )
  }
}

export default App;
