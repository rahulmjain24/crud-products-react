import React from 'react'
import { Link } from 'react-router-dom'
import "./NavBar.css"

class NavBar extends React.Component {
    
    render() { 
        return (
            <nav className="nav">
                <div className="navigation">
                    <Link to="/" className='logo'>Products</Link>
                    <Link to="/create">Create</Link>
                </div>
            </nav>
        )
    }
}
 
export default NavBar