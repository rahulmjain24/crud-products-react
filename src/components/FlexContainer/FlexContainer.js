import React from 'react'
import "./FlexContainer.css"

class FlexContainer extends React.Component {
    render() { 
        return (
            <div className={`container ${this.props.className}`}>
                {this.props.children}
            </div>
        )
    }
}
 
export default FlexContainer;