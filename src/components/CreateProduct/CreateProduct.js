import React from 'react'
import "./CreateProduct.css"
import Button from '../Button/Button'
import validator from 'validator'
import Error from '../Error/Error'
import { Link } from 'react-router-dom'

class CreateProduct extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            title: {value:'', isValid: false, isModified: false},
            category: {value:'', isValid: false, isModified: false},
            price: {value:'', isValid: false, isModified: false},
            description: {value:'', isValid: false, isModified: false},
            isCreated: false
        }
    }

    isValidForm = () => {
        return (
            this.state.title.isValid && 
            this.state.category.isValid &&
            this.state.price.isValid && 
            this.state.description.isValid
        )
    }

    render() {
        return (
            this.state.isCreated ? 
            
                <>
                <Error>
                    New Product has been created
                </Error>
                <Link style={{alignSelf: 'center'}} to='/'><Button change={() => {}} className="green">Go Back</Button></Link>
                <Button 
                    change={() => {
                        this.setState({
                            title: {value:'', isValid: false, isModified: false},
                            category: {value:'', isValid: false, isModified: false},
                            price: {value:'', isValid: false, isModified: false},
                            description: {value:'', isValid: false, isModified: false},
                            isCreated: false
                        })
                    }} 
                    className="blue center">Create More</Button>
            </>
            
            :
            <form
                onSubmit={(e) => {
                    if(this.isValidForm()) { 
                        const {title, category, price, description} = this.state
                        this.props.create({
                            title: title.value,
                            category: category.value,
                            price: parseFloat(price.value),
                            description: description.value
                        })
                        this.setState({isCreated: true})
                    }
                    e.preventDefault()
                }}
                className="form"
            >
                {this.state.isEmpty && <span className='red-text'>The field cannot be empty!!</span>}

                <div className="input-con">
                    <label className="label" htmlFor="title">Title: </label>
                    <textarea
                        onChange={(e) => {
                            if (validator.isEmpty(e.target.value, {
                                ignore_whitespace: true
                            })) {
                                this.setState({ title: {value: e.target.value.trim(), isValid: false, isModified: true }})
                            } else {
                                this.setState({ title: {value: e.target.value.trim(), isValid: true, isModified: true }})
                            }
                        }}
                        className='textarea'
                        id='title'
                        defaultValue={this.props.title}
                    />
                    {!this.state.title.isValid && this.state.title.isModified && <span className="red-text">Title cannot be empty</span>}
                </div>
                <div className="input-con">
                    <label className="label" htmlFor="category">Category: </label>
                    <input
                        onChange={(e) => {
                            if (validator.isEmpty(e.target.value, {
                                ignore_whitespace: true
                            })) {
                                this.setState({ category: {value: e.target.value.trim(), isValid:false, isModified: true }})
                            } else {
                                this.setState({ category: {value: e.target.value.trim(), isValid:true, isModified: true }})
                            }
                        }}
                        className='input'
                        id='category'
                        defaultValue={this.props.category}
                    />
                    {!this.state.category.isValid && this.state.category.isModified && <span className="red-text">Category cannot be empty</span>}
                </div>
                <div className="input-con">
                    <label className="label" htmlFor="description">Description: </label>
                    <textarea
                        onChange={(e) => {
                            if (validator.isEmpty(e.target.value, {
                                ignore_whitespace: true
                            })) {
                                this.setState({ description: {value: e.target.value.trim(), isValid:false, isModified: true }})
                            } else {
                                this.setState({ description: {value: e.target.value.trim(), isValid:true, isModified: true }})
                            }
                        }}
                        className='textarea'
                        id="description"
                        defaultValue={this.props.description}
                        rows='4'
                    />
                    {!this.state.description.isValid && this.state.description.isModified && <span className="red-text">Description cannot be empty</span>}
                </div>
                <div className="input-con">
                    <label className="label" htmlFor="price">Price: </label>
                    <input
                        onChange={(e) => {
                            if (!validator.isNumeric(e.target.value)) {
                                this.setState({ price: {value: e.target.value, isValid:false, isModified: true }})
                            } else {
                                this.setState({ price: {value: e.target.value, isValid:true, isModified: true }})
                            }
                        }}
                        className='input'
                        type="number" 
                        id="price"
                        defaultValue={this.props.price}
                    />
                    {!this.state.price.isValid && this.state.price.isModified && <span className="red-text">Price can't be empty and please enter a valid price</span>}
                </div>
                <Button disabled={!this.isValidForm()} change={() => {}}className="center blue" type='submit'>Create</Button>
            </form>
        )
    }
}

export default CreateProduct