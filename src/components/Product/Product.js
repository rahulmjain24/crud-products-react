import React from 'react'
import Button from '../Button/Button'
import "./Product.css"
import { Link } from 'react-router-dom'
import Error from '../Error/Error'
import validator from 'validator'

class Product extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isBeingEdited: false,
            title: {value:'', isValid: true},
            category: {value:'', isValid: true},
            price: {value:'', isValid: true},
            description: {value:'', isValid: true},
        }
    }

    changeButtonState = () => {
        this.setState((prevState) => {
            return {
                isBeingEdited: !prevState.isBeingEdited
            }
        })
    }

    isValid = () => {
        return (
            this.state.title.isValid && 
            this.state.category.isValid && 
            this.state.price.isValid && 
            this.state.description.isValid
        )
    }

    render() { 
        return (
            this.state.isDeleted ? 
            <>
                <Error>
                    The Product has been deleted, go back to main page to main page
                </Error>
                <Link style={{alignSelf: 'center'}} to='/'><Button change={() => {}} className="green">Go Back</Button></Link>
            </>
            :
            <>
                <img 
                    src="img/arrow.svg"
                    alt="" height="50px" 
                    className='m-2 back' 
                    onClick={() => {
                        this.props.history.goBack()
                    }} 
                />
                <div className="product">
                <div className="product-image">
                    <img src={this.props.image} alt="Product" />
                </div>
                <div className="product-info">
                    {
                        this.state.isBeingEdited ?
                            <>
                                 <div className="input-con">
                                    <label className="label" htmlFor="title">Title: </label>
                                    <textarea 
                                        onChange={(e) => {
                                            if(validator.isEmpty(e.target.value, {
                                                ignore_whitespace: true
                                            })) {
                                                this.setState({ title: {value: '', isValid: false}})
                                            } else {
                                                this.setState({ title: {value: e.target.value.trim(), isValid: true}})
                                            }
                                        }} 
                                        className='textarea' 
                                        id='title' 
                                        defaultValue={this.props.title}
                                    />
                                    {!this.state.title.isValid && <span className='red-text'>Title can't be empty</span>}
                                </div>
                                <div className="input-con">
                                    <label className="label" htmlFor="category">Category: </label>
                                    <input 
                                        onChange={(e) => {
                                            if(validator.isEmpty(e.target.value, {
                                                ignore_whitespace: true
                                            })) {
                                                this.setState({ category: {value: '', isValid: false}})
                                            } else {
                                                this.setState({ category: {value: e.target.value.trim(), isValid: true}})
                                            }
                                        }} 
                                        className='input' 
                                        id='category' 
                                        defaultValue={this.props.category}
                                    />
                                    {!this.state.category.isValid && <span className='red-text'>Category can't be empty</span>}
                                </div>
                                <div className="input-con">
                                    <label className="label" htmlFor="description">Description: </label>
                                    <textarea 
                                        onChange={(e) => {
                                            if(e.target.value === '') {
                                                this.setState({ description: {value: '', isValid: false}})
                                            } else {
                                                this.setState({ description: {value: e.target.value.trim(), isValid: true}})
                                            }
                                        }} 
                                        className='textarea' 
                                        id="description" 
                                        defaultValue={this.props.description} 
                                        rows='4'
                                    />
                                    {!this.state.description.isValid && <span className='red-text'>Description can't be empty</span>}
                                </div>
                                <div className="input-con">
                                <label className="label" htmlFor="price">Price: </label>
                                    <input 
                                        onChange={(e) => {
                                            if(!isNaN(e.target.value) && parseFloat(e.target.value) > 0) {
                                                this.setState({ price: {value: parseFloat(e.target.value), isValid: true}})
                                            } else {
                                                this.setState({ price: {value:'', isValid: false}})
                                            }
                                        }} 
                                        className='input' 
                                        type="number" 
                                        id="price" 
                                        defaultValue={this.props.price} 
                                    />
                                    {!this.state.price.isValid && <span className='red-text'>Price should be a number and greater than 0</span>}
                                </div>
                            </>
                            :
                            <>
                                <div className="title-info">
                                    <div className="title">{this.props.title}</div>
                                    <div className="category">{this.props.category}</div>
                                    <div className="rating">
                                        <span className="rate">
                                            <img className="icon" src="img/star.png" alt="star" />{this.props.rating.rate}
                                        </span>
                                        <span className="count">
                                            <img className="icon" src="img/user.png" alt="user" />{this.props.rating.count}
                                        </span>
                                    </div>
                                </div>
                                <div className="description">{this.props.description}</div>
                                <div className="price">${this.props.price}</div>
                            </>
                    }
                   <div className="buttons">
                        <Button
                            disabled={
                                this.state.isBeingEdited ?
                                !this.isValid()
                                :
                                false
                            }
                            className="blue"
                            change={() => {
                                const {title, category, price, description} = this.state
                                this.changeButtonState()
                                if(title.value !== '' && title.isValid) {
                                    this.props.edit(this.props.id, title.value)
                                }
                                if(description.value !== '' && description.isValid) {
                                    this.props.edit(this.props.id,'',description.value)
                                }
                                if(category.value !== '' && category.isValid) {
                                    this.props.edit(this.props.id,'','',category.value)
                                }
                                if(price.value !== '' && price.isValid) {
                                    this.props.edit(this.props.id,'','','',price.value)
                                }
                            }}
                        >
                        {this.state.isBeingEdited ? 'Save' : 'Edit'}
                        </Button>
                        <Button
                            className="red"
                            change={() => {
                                this.props.delete(this.props.id)
                                this.setState({ isDeleted : true})
                            }}
                        >
                        Delete
                        </Button>
                        <Link to={`/${this.props.id}`}>
                            <Button change={() => {}} className="green">View</Button>
                        </Link>
                    </div>
                </div>
            </div>
            </>
        )
    }
}
 
export default Product