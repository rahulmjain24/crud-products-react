import React from 'react'
import "./Button.css"

class Button extends React.Component {
    render() { 
        return (
            <button
                type={this.props.type || 'button'}
                disabled={this.props.disabled} 
                onClick={(e) => {
                    this.props.change()
                }} 
                className={`button ${this.props.className}`}
            >
                {this.props.children}
            </button>
        )
    }
}
 
export default Button